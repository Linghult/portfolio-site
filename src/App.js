import React, { Component } from 'react';
import './App.css';
import 'react-mdl/extra/material.css';
import 'react-mdl/extra/material.js';
import { Layout, Header, Navigation, Drawer, Content } from 'react-mdl'
import { Link } from 'react-router-dom';
import {
  CSSTransition,
  TransitionGroup,
} from 'react-transition-group';
import { Switch, Route } from 'react-router-dom';
import LandingPage from './Components/landingpage';
import About from './Components/aboutme';
import Contact from './Components/contact';
import Projects from './Components/projects';
import Resume from './Components/resume';



class App extends Component {
  render() {
    return (
      
      <div className="demo-big-content">
        <Layout>
          <Header className="header-color navbar-fixed-top" title={<Link style={{ textDecoration: 'none', color: 'white' }}
            to="/">My Portfolio</Link>} scroll>
            <Navigation>
              <Link to="/resume">Resume</Link>
              <Link to="/aboutme">About Me</Link>
              <Link to="/projects">Projects</Link>
              <Link to="/contact">Contact</Link>
            </Navigation>
          </Header>
          <Drawer title={<Link style={{ textDecoration: 'none', color: 'black' }}
            to="/">MyPortfolio</Link>} scroll>
            <Navigation>
              <Link to="/resume">Resume</Link>
              <Link to="/aboutme">About Me</Link>
              <Link to="/projects">Projects</Link>
              <Link to="/contact">Contact</Link>
            </Navigation>
          </Drawer>
          <Content>
            <div className="page-content">
              <Route render={({ location }) => (
                <TransitionGroup>
                  <CSSTransition
                    key={location.key}
                    timeout={3000}
                    classNames="fade"
                  >
                    <Switch location={location}>
                      <Route exact path="/" component={LandingPage} />
                      <Route exact path="/aboutme" component={About} />
                      <Route exact path="/contact" component={Contact} />
                      <Route exact path="/projects" component={Projects} />
                      <Route exact path="/resume" component={Resume} />
                    </Switch>
                  </CSSTransition>
                </TransitionGroup>
              )} />
            </div>
          </Content>
        </Layout>
      </div>
    );
  }
}

export default App;
