import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';


class LandingPage extends Component {
    render() {
        return (
            <div class="container">
                <div class="row">
                    <div class="landing-grid">
                        <div className="landing-container h-100">
                            <img
                                src="https://i.imgur.com/uZwjSNb.png"
                                alt="avatar"
                                className="avatar-img col-xs-mt-0"
                            />
                            <div className="banner-text  width-sm-100">
                                <h1>
                                    Jens Linghult
                            </h1>
                                <hr />
                                <p>| C# | .NET | Javascript | HTML/CSS | Bootstrap | React |</p>
                                <div className="social-links w-50">

                                    {/*LinkedIN */}
                                    <a href="https://www.linkedin.com/in/jens-linghult-68946122/" rel="noopener noreferrer" target="_blank">
                                        <i className="fa fa-linkedin-square" aria-hidden="true" />
                                    </a>

                                    {/*gitlab*/}
                                    <a href="https://gitlab.com/Linghult" rel="noopener noreferrer" target="_blank">
                                        <i className="fa fa-gitlab" aria-hidden="true" />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default LandingPage;