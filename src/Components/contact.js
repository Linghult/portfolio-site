import React, { Component } from 'react';
import { Grid, Cell, List, ListItemContent, ListItem } from 'react-mdl'

class Contact extends Component {
    render() {
        return (
            <div className="container">
                <div className="contact-grid col-xs-p-0 col-xl-12 h-100">
                    <div className="row mt-5">
                        <div className="col-xl-6">

                            <h2>Jens Linghult</h2>
                            <img
                                className="contact-avatar"
                                src="https://i.imgur.com/gju6KRa.png"
                                alt="avatar"
                            />
                            <h3 className="contact-text">
                                Feel free to contact me for any reason you feel like. I'll most like not be on skype that often, so the safest bet is by phone or E-mail if you want a quick response!
                            </h3>
                        </div>
                        <div className="col-xl-6">
                            <h2>
                                Contact Me
                                </h2>
                            <hr />
                            <div className="contact-list">
                                <List>
                                    <ListItem>
                                        <ListItemContent className='contact-info' style={{ fontSize: '30px', fontFamily: 'Anton' }}>
                                            <i className="fa fa-phone-square" aria-hidden="true" />
                                            (+46) 76-0926757
                                        </ListItemContent>
                                    </ListItem>

                                    <ListItem>
                                        <ListItemContent className='contact-info' style={{ fontSize: '30px', fontFamily: 'Anton' }}>
                                            <i className="fa fa-envelope-square" aria-hidden="true" />
                                            J.Linghult@gmail.com
                                        </ListItemContent>
                                    </ListItem>

                                    <ListItem>
                                        <ListItemContent className='contact-info' style={{ fontSize: '30px', fontFamily: 'Anton' }}>
                                            <i className="fa fa-skype" aria-hidden="true" />
                                            <a>Linghult</a>
                                        </ListItemContent>
                                    </ListItem>

                                </List>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Contact;