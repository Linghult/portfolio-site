import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';

class Education extends Component {
    render() {
        return (
            <Grid>
                <Cell col={4}>

                    <p>{this.props.startYear} - {this.props.endYear}</p>
                </Cell>
                <Cell col={8}>
                    <h3 style={{ marginTop: '0px' }}>{this.props.schoolName}</h3>
                    <p>{this.props.schoolDescription}</p>
                    <p class="subClasses">{this.props.schoolDescription8}</p>
                    <p class="subClasses">  {this.props.schoolDescription2}</p>
                    <p class="subClasses">  {this.props.schoolDescription3}</p>
                    <p class="subClasses">  {this.props.schoolDescription4}</p>
                    <p class="subClasses">  {this.props.schoolDescription5}</p>
                    <p class="subClasses">  {this.props.schoolDescription6}</p>
                    <p class="subClasses">  {this.props.schoolDescription7}</p>
                </Cell>
            </Grid>
        )
    }
}

export default Education;