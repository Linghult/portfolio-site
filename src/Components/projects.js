import React, { Component } from 'react';
import { Tabs, Tab, Grid, Cell, Card, CardTitle, CardText, CardActions, Button, CardMenu, IconButton } from 'react-mdl';


class Projects extends Component {
    constructor(props) {
        super(props);
        this.state = { activeTab: 0 };
    }

    toggleCategories() {

        if (this.state.activeTab === 0) {
                return (
                    <div className="projects-grid">
                        <Card shadow={5} style={{ minWidth: '450', margin: 'auto' }}>
                            <CardTitle style={{ color: '#fff', height: '176px', background: 'url(https://i.imgur.com/9IwzzZV.png) center / cover' }}>Slime Ninja</CardTitle>
                            <CardText>
                                This was a project that me and one other classmate did as a work for school. It took us about 7 weeks and was a blast! It was made with
                                the Unity game engine. If you want to see the game for yourself just give me a call or send and e-mail.
                            </CardText>
                            <CardActions border>
                                <Button href="https://gitlab.com/Linghult/slime-ninja" target="_blank" colored>Git Lab</Button>
                                <Button href="https://drive.google.com/file/d/1SmqqbbLRWdatAR3_zV0SNIB6owNdAbae/view?usp=sharing" target="_blank" colored>Direct Download</Button>
                            </CardActions>
                            <CardMenu style={{ color: '#fff' }}>
                                <IconButton></IconButton>
                            </CardMenu>
                        </Card>   
                    </div>)

        } else if (this.state.activeTab === 1) {
            return (
                <div className="projects-grid">
                    <Card shadow={5} style={{ minWidth: '450', margin: 'auto' }}>
                        <CardTitle style={{ color: '#fff', height: '176px', background: 'url(https://i.imgur.com/Lpf7ABz.png) center / cover' }}>Portfolio Site</CardTitle>
                        <CardText>
                            This site. It was a small project during a couple of days where i wanted to try out React.    
                        </CardText>
                        <CardActions border>
                            <Button href="https://gitlab.com/Linghult/portfolio-site" target="_blank" colored>Git Lab</Button>
                        </CardActions>
                        <CardMenu style={{ color: '#fff' }}>
                            <IconButton></IconButton>
                        </CardMenu>
                    </Card>               
                </div>)
                
        } else if (this.state.activeTab === 2) {
            return (
                <div className="projects-grid">
                <Card shadow={5} style={{ minWidth: '450', margin: 'auto' }}>
                        <CardTitle style={{ color: '#fff', height: '176px', background: 'url(https://i.imgur.com/Xt6uCLS.png) center / cover' }}>Presenthjälp</CardTitle>
                    <CardText>
                        A project that me and two of my class mates did during ~3 months with fully functioning database interaction. It's a site where you 
                        can upload your wishing list for presents to any occasion and let others either contribute money to buying them or taking on buying 
                        the present on their own. Sadly its abit broken right now since it got rehosted.
                        </CardText>
                    <CardActions border>
                            <Button href="https://presenthjalp.com" target="_blank" colored>Go to site</Button>
                    </CardActions>
                    <CardMenu style={{ color: '#fff' }}>
                        <IconButton></IconButton>
                    </CardMenu>
                </Card>
                </div>)
        } 
    }
    render() {
        return (
            <div>
                <Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })} ripple>
                    <Tab>C#</Tab>
                    <Tab>REACT</Tab>
                    <Tab>Laravel</Tab>
                </Tabs>
                <Grid>
                    <Cell col={12}>
                        <div className="content">{this.toggleCategories()}</div>
                    </Cell>
                </Grid>
            </div>
        )
    }
}
export default Projects;