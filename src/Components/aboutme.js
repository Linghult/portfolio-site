import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';


class About extends Component {
    render() {
        return (
            <div className="about-body container">
                <div class="row">
                    <Grid className="about-grid overflow-auto p-0">
                        <div className="about-headline col-12 mb-3 col-xs-mb-0">
                            <h1 className="about-headline mb-4">About me</h1>
                        </div>
                        <div className="about-text col-xs-12 col-xl-6 mt-0">
                            <h3 className="mt-0">
                                I'm a giant tech nerd who ~2 years ago began on the journey to become a system developer. I've tried out many professions during the last 12 years and believe I’ve finally found what I want to work with for the unforeseeable future.
                                When I don’t dream about code I spend alot of my time with games and sofa entertainment with our lovely cats Kimchi and Maki.
                                And to counter the stationary times I spend a couple of moments each week going to the gym, playing disc golf or squash.
                                <br />
                                <br />
                                Since my family got our first hand-me-down computer from my grandparents at age five I have been more or less glued to the screen in one way or another.
                                It started off as a love for gaming and branched out to photo editing and working out problems with computers for family and friends.
                                As much as I love the part where you build computers and troubleshoot them I have discovered that creating the software is what is the most enjoyable.
                                <br />
                                <br />
                                As I am at the start of my career I’m open to opportunities to learn new things
                                as a system developer – be it designing beautiful websites or how to code backend.
                                The site you're on now is going to be my canvas as I learn more about web development and I will surely upload more projects where I’ve tried out or honed my skills in other areas.
                            </h3>
                        </div>
                    </Grid>
                </div>
            </div>
        )
    }
}

export default About;