import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';
import Education from './education';
import Experience from './experience';
import Skills from './skills';

class Resume extends Component {
    render() {
        return (
            <div>
                <Grid>
                    <Cell col={4}>
                        <div className='resume-avatar'>
                            <img
                                src="https://i.imgur.com/uZwjSNb.png"
                                alt="avatar"
                            />

                        </div>
                        <h2 style={{ paddingTop: '0.5em' }}>Jens Linghult</h2>
                        <h4 style={{ color: 'grey' }}>System Developer</h4>
                        <hr style={{ borderTop: '3px solid #B33fb2', width: '50%' }}></hr>
                        <p>I'm a programmer that is on my last year of school as a system developer in a two year program in the school TUC. We've studied a broad set of subjects regarding system development as you can see on the right. I was new to
                            programming when i started so anything programming related that you can see to the right have been learnt during the last two years.
                            I have long searched for a job that i feel that i can settle down with and i've tried many things during my grown life and i feel that i've finally found what i love!
                        </p>
                        <hr style={{ borderTop: '3px solid #B33fb2', width: '50%' }}></hr>
                        <h5>Address</h5>
                        <p>Bråddgatan 54</p>
                        <p>602 20 Norrköping</p>
                        <h5>Phone</h5>
                        <p>(+46) 76-0926757</p>
                        <h5>Email</h5>
                        <p>J.Linghult@gmail.com</p>

                    </Cell>
                    <Cell className="resume-right-col m-0 p-0" col={8}>
                        <h2>Education</h2>
                        <Education
                            startYear={2017}
                            endYear={2019}
                            schoolName="TUC"
                            schoolDescription=".NET system developer"
                            schoolDescription8="-Web applications and mobile development (Javascript, HTML, CSS)"
                            schoolDescription2="-Web design and user experience"
                            schoolDescription3="-Cloud architecture"
                            schoolDescription3="-Publishing tools"
                            schoolDescription4="-Agile software development"
                            schoolDescription5="-Information architecture and database development"
                            schoolDescription6="-IT security for developers"
                            schoolDescription7="-Test, verification and certification"
                        />

                        <Education
                            startYear={2009}
                            endYear={2011}
                            schoolName="TEC"
                            schoolDescription="Travel manager"
                        />
                        <Education
                            startYear={2005}
                            endYear={2008}
                            schoolName="IT-Gymnasiet"
                            schoolDescription="IT and media focused"
                        />
                        <hr style={{ borderTop: '3px solid #CC797E' }} />

                        <h2>Experience</h2>

                        <Experience
                            startYear={2019}
                            endYear={2019}
                            jobName="Skyfox Interactive"
                            jobDescription="Internship. Game development in C# and the Unity framework"
                        />

                        <Experience
                            startYear={2018}
                            endYear={2018}
                            jobName="Emmio web agency"
                            jobDescription="Internship working on a site with the Laravel framework"
                        />
                        <Experience
                            startYear={2016}
                            endYear={2017}
                            jobName="Skola"
                            jobDescription="Substitute teacher"
                        />
                        <Experience
                            startYear={2016}
                            endYear={2017}
                            jobName="Kriminalvården"
                            jobDescription="Warden"
                        />

                        <Experience
                            startYear={2014}
                            endYear={2015}
                            jobName="Sony Playstation"
                            jobDescription="Event Personnel"
                        />

                        <Experience
                            startYear={2011}
                            endYear={2013}
                            jobName="Gallerix"
                            jobDescription="sales staff and post manager"
                        />
                        <Experience
                            startYear={2010}
                            endYear={2011}
                            jobName="Regina Hotel"
                            jobDescription="Bartender and waiter"
                        />

                        <Experience
                            startYear={2009}
                            endYear={2009}
                            jobName="Apollo"
                            jobDescription="Tour manager on Crete"
                        />

                        <hr style={{ borderTop: '3px solid #CC797E' }} />

                        <h2>Skills</h2>
                        <Skills
                            skill="javascript"
                            progress={20}
                        />
                        <Skills
                            skill="HTML/CSS"
                            progress={60}
                        />
                        <Skills
                            skill="REACT"
                            progress={20}
                        />
                        <Skills
                            skill="C#"
                            progress={40}
                        />
                        <Skills
                            skill="Windows"
                            progress={80}
                        />
                        <Skills
                            skill="Word"
                            progress={80}
                        />
                        <Skills
                            skill="Excel"
                            progress={70}
                        />
                        <Skills
                            skill="Photoshop"
                            progress={50}
                        />


                    </Cell>

                </Grid>
            </div>
        )
    }
}

export default Resume;